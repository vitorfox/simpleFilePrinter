FROM node:7-alpine
RUN mkdir /application
ADD package.json /application
RUN npm install
ADD . /application
WORKDIR /application
CMD ["node", "index.js"]
