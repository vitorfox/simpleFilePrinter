var express = require("express")
var app = express()

var file = process.env.FILE
fs = require('fs')
fs.readFile(file, 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }
  app.get('/*', function (req, res) {
   res.send(data)
  })
  app.listen(3000)
});
